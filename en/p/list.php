<?php
	$subdirs = scandir('/vm', SCANDIR_SORT_ASCENDING);
	$protocol = $_SERVER['HTTPS'] ? 'https://' : 'http://';
	foreach ($subdirs as $subdir) {
		if ($subdir != '.' && $subdir != '..') {
			$ph_index = '/sshfs/home/' . $subdir . '/public_html/index.';
			if (is_readable('/vm/' . $subdir . '/.pgp.asc'))
				echo $protocol . $_SERVER['HTTP_HOST'] . "/p/$subdir.asc\n";
		}
	}
?>
