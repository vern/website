<!DOCTYPE html>
<!--
    This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
-->
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="A list of ~vern users' PGP keys">
	<link rel="stylesheet" href="//gcdn.vern.cc/vernsite/style.css">
	<title>PGP | ~vern</title>
</head>
<body>
	<?php
		$list = '';

		$subdirs = scandir('/vm', SCANDIR_SORT_ASCENDING);
		foreach ($subdirs as $subdir) {
			if ($subdir != '.' && $subdir != '..') {
				$ph_index = '/sshfs/home/' . $subdir . '/public_html/index.';
				if (is_readable('/vm/' . $subdir . '/.pgp.asc'))
					$list .= "	<li><a href='/p/$subdir.asc'>$subdir</a></li>\n";
			}
		}
	?>
	<!--#include file="/en/nav.php" -->
	<div class=h><h1 id=pgp>User PGP keys</h1> <a aria-hidden=true href=#pgp>#pgp</a></div>
	<p>This is a list of users with PGP keys on ~vern.</p>
	<ol>
		<?php echo $list; ?>
	</ol>
	<!--#include file="/en/footer.cgi" -->
</body>
</html>
