<!DOCTYPE html>
<!--
    This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
-->
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="A list of ~vern users">
	<link rel="stylesheet" href="//gcdn.vern.cc/vernsite/style.css">
	<title>Users | ~vern</title>
</head>
<body>
	<?php
		$total = $on = 0;
		$list = '';
		$onlist = array();
		$users = explode(' ', shell_exec('users /sshfs/var/run/utmp'));

		$subdirs = scandir('/vm', SCANDIR_SORT_ASCENDING);
		foreach ($subdirs as $subdir) {
			$online = '<b><span class=';
			if ($subdir != '.' && $subdir != '..') {
				$is_online = false;
				foreach ($users as $val)
					if (trim($val) == $subdir && !in_array($subdir, $onlist)) {
						$online .= 'green';
						$is_online = true;
						$on++;
						array_push($onlist, $subdir);
					}
				if (!$is_online) $online .= 'red';
				$online .= '>•</span></b> ';
				$list .= '	<li>' . $online;
				
				$ph_index = '/sshfs/home/' . $subdir . '/public_html/index.';
				if (is_readable($ph_index . 'html') ||
				    is_readable($ph_index . 'php')  ||
				    is_readable($ph_index . 'cgi')  ||
				    is_readable($ph_index . 'py')   ||
				    is_readable($ph_index . 'sh')   ||
				    is_readable($ph_index . 'pl')   ||
				    is_readable($ph_index . 'lua')) // There must be a better way to do this
					$list .= "<a href='//$_SERVER[HTTP_HOST]/~$subdir/'>$subdir</a>";
				else $list .= $subdir;
				$ph_index = '/sshfs/home/' . $subdir . '/public_gemini/index.';
				if (is_readable($ph_index . 'gmi')  ||
				    is_readable($ph_index . 'cgi')  ||
				    is_readable($ph_index . 'py')   ||
				    is_readable($ph_index . 'sh')   ||
				    is_readable($ph_index . 'pl')   ||
				    is_readable($ph_index . 'lua')) // There must be a better way to do this
					$list .= " (<a href='//gp.vern.cc/gemini/$subdir.vern.cc/'>gemini</a>)";
				$ph_index = '/sshfs/home/' . $subdir . '/public_gopher/gopher';
				if (is_readable($ph_index . 'map')) // There must be a better way to do this
					$list .= " (<a href='//gp.vern.cc/gopher/vern.cc/1/~$subdir/'>gopher</a>)";
				$list .= "</li>\n";
				$total++;
			}
		}
	?>
	<!--#include file="nav.php" -->
	<div class=h><h1 id=users>Users</h1> <a aria-hidden=true href=#users>#users</a></div>
	<p>This is a list of all ~vern users, and whether they are online (<b><span class=green>•</span></b>) or not (<b><span class=red>•</span></b>) in alphabetical order. There are currently a total of <?php echo $total; ?> members, <?php echo $on; ?> of which <?php echo $on !== 1 ? "are" : "is" ?> online.</p>
	<ol>
		<?php echo $list; ?>
	</ol>
	<!--#include file="footer.cgi" -->
</body>
</html>
