<!--<link rel=stylesheet href="//gcdn.vern.cc/vernsite/style.css">-->
<header>
	<a href=/en/><img src="/media/verntrans.svg" alt="~V" class="navlogo"></a>
	<nav>
		<a href="//status.vern.cc">Status</a>
		<a href="/users">Users</a>
		<a href="/register">SignUp</a>
		<a href="//wiki.vern.cc">Wiki</a>
		<a href="/blog">Blog</a>
		<a href="/faq">FAQ</a>
		<a href="/rules">Rules</a>
		<a href="/privpol">Privacy</a>
		<a href="/donate">Donate</a>
		<a aria-label="RSS" href="/blog/feed.xml"><i style="color: #EE802F;" data-content="RSS Feed" data-position="top center" data-variation="inverted tiny"><svg style="display: inline-block; vertical-align: text-top; fill: currentcolor;" viewBox="0 0 16 16" class="svg octicon-rss" width="18" height="18" aria-hidden="true"><path fill-rule="evenodd" d="M2.002 2.725a.75.75 0 0 1 .797-.699C8.79 2.42 13.58 7.21 13.974 13.201a.75.75 0 1 1-1.497.098 10.502 10.502 0 0 0-9.776-9.776.75.75 0 0 1-.7-.798zM2 13a1 1 0 1 1 2 0 1 1 0 0 1-2 0zm.84-5.95a.75.75 0 0 0-.179 1.489c2.509.3 4.5 2.291 4.8 4.8a.75.75 0 1 0 1.49-.178A7.003 7.003 0 0 0 2.838 7.05z"></path></svg></i></a>
	</nav>
</header>
<hr>
