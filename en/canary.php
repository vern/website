<!DOCTYPE html>
<!--
    This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
-->
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="Warrant Canary of ~vern">
	<link rel="stylesheet" href="//gcdn.vern.cc/vernsite/style.css">
	<title>Warrant Canary | ~vern</title>
</head>
<body>
	<!--#include file="nav.php" -->
	<div class=h><h1 id=canary>Warrant Canary</h1> <a aria-hidden=true href=#canary>#canary</a></div>
	<p>Below is our <a href=//wl.vern.cc/wiki/warrant_canary>warrant canary</a>. Please watch very closely for any updates of this page. We release a new canary monthly, within a week of the beginning of the month.</p>
	<pre><code><?php echo file_get_contents("/var/log/canary/latest"); ?></code></pre>
	<p>This canary's SHA256 checksum is <?php echo hash('sha256', file_get_contents("/var/log/canary/latest")); ?></p>

	<p>Previous canaries can be found <a href=/canaries>here</a>.</p>
	<!--#include file="footer.cgi" -->
</body>
</html>

