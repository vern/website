<!DOCTYPE html>
<!--
    This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
-->
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="Donations page for ~vern">
	<link rel="stylesheet" href="//gcdn.vern.cc/vernsite/style.css">
	<title>Donations | ~vern</title>
</head>
<body>
	<!--#include file="nav.php" -->
	<div class=h><h1 id=donate>Donations</h1> <a aria-hidden=true href=#donate>#donate</a></div>
	<p>A donation would help ~vern stay up and running for longer! We currently only take donations in XMR and LiberaPay.</p>
	<p>If you know of another good way to take donations, either in cryptocurrency or traditional banking, please contact an <a href=/en/admins>admin</a> about adding it.</p>
	<p>As a thank you token, donators get access to restricted access services such as Jellyfin and can also opt to use mail with their own custom domain on ~vern.</p>
	<p><a href=//liberapay.com/vern>Liberapay</a> accepts donations in many currencies, more info can be found at <a href=//liberapay.com/about/global>liberapay.com</a>.</p>
	<p>We have an XMR OpenAlias on vern.cc, so simply type vern.cc into the address bar and click resolve.</p>
	<p>Our XMR address is <code>4ATm8EZaC4iEuwt1VABuxFPi5xq4HgvqgghmKqvgQyvwQf9KK7AyYFXCYCZ3xBiqZ38CctAWihJ3yLeKAeGJfQBgDxQnn91</code></p>
	<img src="/media/xmr.png" alt="A QR code representing our XMR address" width=300 height=300>
	<p> A transparent report on how the donations money is spent can be found in <a href=/en/costs>/costs</a>

	<?php
		$success = false;

		$name = $proof = $message = $address = $public =
			$name_err = $proof_err = $message_err = $address_err = '';

		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			if (!empty($_POST['name'])) {
				$name = $_POST['name'];
			} else {
				$name_err = "Name is required";
			}

			if (!empty($_POST['proof'])) {
				$proof = $_POST['proof'];
			} else {
				$proof_err = "Proof is required";
			}

			if (!empty($_POST['address'])) {
				$address = $_POST['address'];
			} else {
				$address_err = "Address is required";
			}

			if (!empty($_POST['message'])) {
				$message = $_POST['message'];
			} else {
				$message_err = "Message is required";
			}

			$public = $_POST['public'];

			if (empty($name_err . $proof_err . $message_err . $address_err))
				$success = true;
		}

		if (!$success) {
	?>
		<div class=h><h2 id=message>Leave a message with your donation</h2> <a aria-hidden=true href=#message>#message</a></div>
		<span class="red">* Required field</span>
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
			<p>Name:
			<input type="text" name="name">
			<span class="red">* <?php echo $name_err; ?></span></p>

			<p>Payment proof or transaction key (<a href=https://www.getmonero.org/resources/user-guides/prove-payment.html>getmonero.org/resources/user-guides/prove-payment</a>):
			<input type="password" name="proof">
			<span class="red">* <?php echo $proof_err; ?></span></p>

			<p>Address:
			<input type="password" name="address">
			<span class="red">* <?php echo $address_err; ?></span></p>

			<p>Message: <span class="red">* <?php echo $message_err; ?></span><br>
			<textarea name="message" rows="8" cols="50"></textarea></p>

			<input type="checkbox" id="public" name="public" value="Public">
			<label for="public">Can we show this message to the public?</label><br>

			<br>
			<span><input type="submit" value="Submit" style="width:100px;height:40px;font-size:20px"></span>
		</form>
	<?php
		} else {
	?>
			<meta http-equiv="refresh" content="5;url=http://<?php echo htmlspecialchars($_SERVER['HTTP_HOST']); ?>" />
			<div class=h><h1 id=thanks>Thank you for leaving a message.</h1> <a aria-hidden=true href=#thanks>#thanks</a></div>
			<p>An admin will review your request, and an e-mail will be sent if your message is valid.</p>
			<p>You will be redirected back <a href=/en/>home</a> in 5 seconds.</p>

	<?php
			$to = "root@vern.cc";
			$subject = "New donation message from $name <$address>";
			$email = "Hello Administrators,\nSomeone has sent a donation message. Please review to make sure it is legitimate.";
			if ($public)  $email .= " They allow this to be public.";
			$email .= "\n\nAddress: $address\n";
			$email .= "Proof:   $proof\n";
			$email .= "\nMessage:\n$message\n";

			$from = "donations@vern.cc";
			$headers = "From: " . $from . "\n";
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-type: text/plain\n";
			$headers .= "Cc: Skylar <cobra@vern.cc>\n";
			$headers .= "Cc: Arya <aryak@vern.cc>\n";
			$headers .= "Cc: Neo <neopenk@vern.cc>\n";

			mail($to, $subject, $email, $headers);
		}
	?>

	<!--#include file="footer.cgi" -->
</body>
</html>
