<!DOCTYPE html>
<!--
    This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
-->
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="Statistics of ~vern">
	<link rel="stylesheet" href="//gcdn.vern.cc/vernsite/style.css">
	<title>Statistics | ~vern</title>
</head>
<body>
	<!--#include file="nav.php" -->
	<div class=h><h1 id=stats>Statistics</h1> <a aria-hidden=true href=#stats>#stats</a></div>
	<p>Below are several graphs for network and power usage for many of our servers.</p>

	<div class=h><h2 id=tilserv>Tildeserver</h2> <a aria-hidden=true href=#tilserv>#tilserv</a></div>
	<?php echo file_get_contents("./gnuplot/net/" . date("Y-m-d") . ".svg"); ?>
	<?php echo file_get_contents("./gnuplot/power/" . date("Y-m-d") . ".svg"); ?>

	<div class=h><h2 id=mythos>Mythos</h2> <a aria-hidden=true href=#mythos>#mythos</a></div>
	<?php echo file_get_contents("./gnuplot/mythos/" . date("Y-m-d") . ".svg"); ?>

	<div class=h><h2 id=iceberg>Iceberg</h2> <a aria-hidden=true href=#iceberg>#iceberg</a></div>
	<?php echo file_get_contents("./gnuplot/iceberg/" . date("Y-m-d") . ".svg"); ?>

	<div class=h><h2 id=zodiac>Zodiac</h2> <a aria-hidden=true href=#zodiac>#zodiac</a></div>
	<?php echo file_get_contents("./gnuplot/zodiac/net/" . date("Y-m-d") . ".svg"); ?>
	<?php echo file_get_contents("./gnuplot/zodiac/power/" . date("Y-m-d") . ".svg"); ?>

	<div class=h><h2 id=crescent>Crescent</h2> <a aria-hidden=true href=#crescent>#crescent</a></div>
	<?php echo file_get_contents("./gnuplot/crescent/net/" . date("Y-m-d") . ".svg"); ?>
	<?php echo file_get_contents("./gnuplot/crescent/power/" . date("Y-m-d") . ".svg"); ?>

	<!--#include file="footer.cgi" -->
</body>
</html>

