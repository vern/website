# ~vern's website

This repo contains the files for the ~vern website.

It is completely static other than the 8 php scripts licensed under a GNU Affero General Public License, version 3. The generated content is licensed under a CC BY-SA 4.0 International license, along with all other content on the website

To start a version locally, you can use https://github.com/danvk/ssi-server or nginx configs similar to vern/nginx-configs:/common/website.conf.
